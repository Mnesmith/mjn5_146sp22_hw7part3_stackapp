import java.util.Arrays;
/**
 *
 * @author marlayshanesmith
 */
public class StackApp {
    
    public static void main(String args[]) {
        
        GenericStack<String> c = new GenericStack<>();
        
            String[] array = new String[] {"Bag of chips expiring April 17", 
                            "Bag of chips expiring April 14", 
                            "Bag of chips expiring April 11"};
    for (String print: array) {
        System.out.println("Push onto stack: " + array.push() );
        }  
        System.out.println("The stack contains " + array.size() + " items\n");
    
        System.out.println("Peek (at top of stack): " + array.peek());
        System.out.println("The stack contains " + array.size() + " items\n");
        
         while( array > array.size() ){
            array.pop();
            System.out.println("Pop from stack: ");
        }
           
        System.out.println("The stack contains " + array.size() + " items");
        
    } // end main method
    
} // end class GenericStack
