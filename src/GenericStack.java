
import java.util.ArrayList;



/**
 *
 * @author mjn5@email.sc.edu
 * @param <E>
 */
public class GenericStack<E> {
    
    private ArrayList<E> backingList = new ArrayList<>();
    
    public int size() {
        return backingList.size();
    } // end method size
    
    public E peek() {
      return backingList.get(backingList.size() - 1);
    } // end method peek
    
    public E pop() {
        return backingList.remove(0);
    } // end method pop
    
     public void push(E item) {
        backingList.add(item);
    } // end method push
    
} // end class GenericStack
